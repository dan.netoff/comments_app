from django.shortcuts import render,  HttpResponseRedirect
from django.views.generic import ListView, DetailView
from .models import Comment
from .forms import NewCommentForm


class MainCommentListView(ListView):
    model = Comment
    template_name = 'comments/comments_list.html'
    context_object_name = 'comments'

    def get_queryset(self):
        return Comment.objects.filter(is_main=True).order_by('-date_add')


class CommentListView(ListView):
    model = Comment
    template_name = 'comments/comments_detail.html'
    context_object_name = 'comments'

    def post(self, request, main_id):
        main = Comment.objects.filter(id=main_id).first()
        comment_form = NewCommentForm(main_id, request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.main = main
            comment.save()
        return HttpResponseRedirect('/comment/' + str(main_id))

    def get_queryset(self):
        main = Comment.objects.filter(pk=self.kwargs['main_id']).first()
        # Combines two QuerySets to TreeQuerySet for mptt
        return Comment.objects.filter(id=self.kwargs['main_id']) | main.comments.all()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        comment_form = NewCommentForm(main_id=self.kwargs['main_id'])
        context['comment_form'] = comment_form
        return context


