from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Comment(MPTTModel):
    username = models.CharField('username', max_length=50)
    # user_ip = models.CharField('user_ip', max_length=15)
    # user_browser = models.CharField('user_browser', max_length=50)
    # email = models.EmailField('email', max_length=100)
    # homepage = models.URLField('homepage', max_length=100)
    text = models.TextField('text')
    date_add = models.DateTimeField('date_add', auto_now_add=True)
    is_main = models.BooleanField('is_main', default=False)

    main = models.ForeignKey('self', on_delete=models.CASCADE, related_name='comments', null=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['date_add']

    def __str__(self):
        return 'Comment ('+str(self.text)+') by '+str(self.username)

    def get_absolute_url(self):
        return reverse('comments-list', kwargs={'id': self.id})
