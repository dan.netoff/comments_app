from django import forms
from .models import Comment
from mptt.forms import TreeNodeChoiceField
from django_markdown.widgets import MarkdownWidget


class NewCommentForm(forms.ModelForm):
    text = forms.CharField(widget=MarkdownWidget())

    class Meta:
        model = Comment
        fields = ('username', 'parent', 'text')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'col-sm-12'}),
            'text': forms.Textarea(attrs={'class': 'form-control'})
        }

    def __init__(self, main_id, *args, **kwargs):
        super(NewCommentForm, self).__init__(*args, **kwargs)
        main = Comment.objects.get(id=main_id)
        self.fields['parent'] = TreeNodeChoiceField(queryset=(Comment.objects.filter(id=main_id) | main.comments.all()))
        self.fields['parent'].required = False

