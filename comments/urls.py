from django.urls import path
from .views import MainCommentListView, CommentListView

urlpatterns = [
    path('', MainCommentListView.as_view(), name='main-comments-list'),
    path('comment/<int:main_id>/', CommentListView.as_view(), name='comments-list')
]
